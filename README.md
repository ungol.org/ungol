UNGOL
=====

UNGOL (UNG On Linux) is a Linux distribution using the UNG (UNG's Not GNU)
userland. The UNG userland is a strict implementation of the Single UNIX
Specification. See https://ung.org/ for more information on UNG.

UNGOL-specific tools
--------------------
While the UNG userland is meant to be portable, the implementation of some
utilities requires system specific details in order to function properly. When
feasible, there are minimal UNGOL-specific variants of those tools. These tools
include:

- init, the process that brings the system up to a running state
- ps, for listing processes (implementation of which requires non-portable
  parsing of files in /proc)

External Sources
----------------
Some tools required for a working Linux system are too different from their
UNG equivalents (or have no UNG equivalent) to warrant porting effort. In
these cases, UNGOL uses upstream versions of:

- Linux, the kernel itself
- kmod, the Linux kernel module utility
- syslinux, a bootloader

Note that while the UNGOL distribution, as well as all UNGOL-specific code and
UNG userland, is under the license described in LICENSE, external sources are
under their own license as described in their respective directories.

Building UNGOL
==============
You will need a C compiler, a C library that supports static linking (UNGOL
does not yet support dynamic linking), lex, yacc, and make.

Dependencies
------------
All source dependencies are included as git submodules. To install them, just
run:

    git submodule update --init

In addition to the build tools, you will need genisoimage to build the ISO
image

Configuration
-------------
There is no configuration required. The kernel is built with defaults. Other
external tools have their configuration included in the Makefile.

Compilation
-----------
From the top level of the source tree, run:

    make

That's it. You'll get a bootable CD image called `ungol.iso`.
