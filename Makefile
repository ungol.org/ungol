.POSIX:

LINUX_VER=5.17.4

CC=c99
CFLAGS=-Wall -Wextra -g -static
BINDIR=root/bin

all: ungol.iso

COMPLEX_BINS=\
	$(BINDIR)/ed \
	$(BINDIR)/file \
	$(BINDIR)/ps

PRINTF_LINK=
#$(BINDIR)/echo

BIN_PROGRAMS=$(COMPLEX_BINS)

EMPTY_DIRS=root/dev/pts root/tmp root/sys/linux/proc root/var/secure/users/root root/etc root/sys/linux/sys root/bin

ungol.iso: $(EMPTY_DIRS)
ungol.iso: $(COMPLEX_BINS)

ungol.iso: 
	genisoimage -J -r -input-charset=utf-8 -quiet -o $@ -b boot/syslinux/isolinux.bin -c boot/syslinux/boot.cat -no-emul-boot -boot-info-table root

qemu: ungol.iso
	qemu-system-x86_64 -display curses -cdrom ungol.iso $$(test -f hda.img && echo '-hda hda.img')

serial: ungol.iso
	qemu-system-x86_64 -nographic -cdrom ungol.iso $$(test -f hda.img && echo '-hda hda.img')

$(EMPTY_DIRS):
	mkdir -p $@

$(COMPLEX_BINS):
	make -f tools.mk COMPLEX_BIN=$(@F) $@

$(PRINTF_LINK): $(BINDIR)/printf
	ln -f $(BINDIR)/printf $@

clean:
	rm -rf root ungol.iso
	find src -name linux -prune -o -name \*.o -exec rm -f {} +
	for i in $(COMPLEX_BINS); do cd src/$$(basename $$i); make clean; cd ../../; done

include mk/deps.mk
