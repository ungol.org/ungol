.POSIX:

ungol.iso: $(BINDIR)/sh

SH_LINKS=\
	alias \
	bg \
	cd command \
	eval exec exit export \
	false fc fg \
	getopts \
	hash \
	kill \
	newgrp \
	pwd \
	read readonly \
	set \
	times trap true type \
	ulimit umask unset \
	wait

$(BINDIR)/sh: src/sh/sh
	cp -f src/sh/sh $@
	for i in $(SH_LINKS); do cd $(BINDIR); ln -sf sh $$i; cd -; done

src/sh/sh:
	cd src/sh; make CC="$(CC)" CFLAGS="$(CFLAGS)" LDFLAGS="$(LDFLAGS)"
	

clean: sh-clean
sh-clean:
	cd src/sh; make clean
