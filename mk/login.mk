.POSIX:

ungol.iso: $(BINDIR)/login

$(BINDIR)/login: src/login/login.c
	$(CC) -static -o $@ src/login/login.c -lcrypt

clean: login-clean
login-clean:
	rm -f $(BINDIR)/login
