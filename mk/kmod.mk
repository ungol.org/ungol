.POSIX:

ungol.iso: $(BINDIR)/kmod

$(BINDIR)/kmod: src/kmod/Makefile
	cd src/kmod; make

src/kmod/Makefile: src/kmod/configure
	cd src/kmod; ./configure

src/kmod/configure: src/kmod/autogen.sh
	cd src/kmod; sh autogen.sh

clean: kmod-clean

kmod-clean:
	cd src/kmod; make clean
