.POSIX:

ungol.iso: root/boot/syslinux/isolinux.bin
ungol.iso: root/boot/syslinux/ldlinux.c32
ungol.iso: root/boot/syslinux/syslinux.cfg

root/boot/syslinux/syslinux.cfg: etc/syslinux.cfg
	@mkdir -p $(@D)
	sed -e 's/LINUX_VER/$(LINUX_VER)/g' etc/syslinux.cfg > $@

root/boot/syslinux/isolinux.bin: src/syslinux/bios/core/isolinux.bin
	@mkdir -p $(@D)
	cp -f src/syslinux/bios/core/isolinux.bin $@

src/syslinux/bios/core/isolinux.bin src/syslinux/bios/com32/elflink/ldlinux/ldlinux.c32:
	cd src/syslinux; patch -p1 < ../../patches/syslinux.diff
	cd src/syslinux; make

root/boot/syslinux/ldlinux.c32: src/syslinux/bios/com32/elflink/ldlinux/ldlinux.c32
	@mkdir -p $(@D)
	cp -f src/syslinux/bios/com32/elflink/ldlinux/ldlinux.c32 $@

syslinux-clean:
	cd src/syslinux; make spotless
	cd src/syslinux; git reset --hard syslinux-6.04-pre1
