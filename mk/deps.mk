.POSIX:

default:
	@echo "Do not use this Makefile directly, it is included from the top-level"

ungol.iso: $(BINDIR)/asa
$(BINDIR)/asa: src/asa/asa.c
	$(CC) -o $@ $(CFLAGS) src/asa/asa.c

ungol.iso: $(BINDIR)/basename
$(BINDIR)/basename: src/basename/basename.c
	$(CC) -o $@ $(CFLAGS) src/basename/basename.c

ungol.iso: $(BINDIR)/cal
$(BINDIR)/cal: src/cal/cal.c
	$(CC) -o $@ $(CFLAGS) src/cal/cal.c

ungol.iso: $(BINDIR)/cat
$(BINDIR)/cat: src/cat/cat.c
	$(CC) -o $@ $(CFLAGS) src/cat/cat.c

ungol.iso: $(BINDIR)/chgrp
$(BINDIR)/chgrp: src/chgrp/chgrp.c
	$(CC) -o $@ $(CFLAGS) src/chgrp/chgrp.c

ungol.iso: $(BINDIR)/chmod
$(BINDIR)/chmod: src/chmod/chmod.c
	$(CC) -o $@ $(CFLAGS) src/chmod/chmod.c

ungol.iso: $(BINDIR)/chown
$(BINDIR)/chown: src/chown/chown.c
	$(CC) -o $@ $(CFLAGS) src/chown/chown.c

ungol.iso: $(BINDIR)/cksum
$(BINDIR)/cksum: src/cksum/cksum.c
	$(CC) -o $@ $(CFLAGS) src/cksum/cksum.c

ungol.iso: $(BINDIR)/cmp
$(BINDIR)/cmp: src/cmp/cmp.c
	$(CC) -o $@ $(CFLAGS) src/cmp/cmp.c

ungol.iso: $(BINDIR)/cp
$(BINDIR)/cp: src/cp/cp.c
	$(CC) -o $@ $(CFLAGS) src/cp/cp.c

ungol.iso: $(BINDIR)/date
$(BINDIR)/date: src/date/date.c
	$(CC) -o $@ $(CFLAGS) src/date/date.c

ungol.iso: $(BINDIR)/dd
$(BINDIR)/dd: src/dd/dd.c
	$(CC) -o $@ $(CFLAGS) src/dd/dd.c

ungol.iso: $(BINDIR)/dirname
$(BINDIR)/dirname: src/dirname/dirname.c
	$(CC) -o $@ $(CFLAGS) src/dirname/dirname.c

ungol.iso: $(BINDIR)/du
$(BINDIR)/du: src/du/du.c
	$(CC) -o $@ $(CFLAGS) src/du/du.c

ungol.iso: $(BINDIR)/env
$(BINDIR)/env: src/env/env.c
	$(CC) -o $@ $(CFLAGS) src/env/env.c

ungol.iso: $(BINDIR)/expand
$(BINDIR)/expand: src/expand/expand.c
	$(CC) -o $@ $(CFLAGS) src/expand/expand.c

ungol.iso: $(BINDIR)/getconf
$(BINDIR)/getconf: src/getconf/getconf.c
	$(CC) -o $@ $(CFLAGS) src/getconf/getconf.c

ungol.iso: $(BINDIR)/getty
$(BINDIR)/getty: src/getty/getty.c
	$(CC) -o $@ $(CFLAGS) src/getty/getty.c

ungol.iso: $(BINDIR)/grep
$(BINDIR)/grep: src/grep/grep.c
	$(CC) -o $@ $(CFLAGS) src/grep/grep.c

ungol.iso: $(BINDIR)/head
$(BINDIR)/head: src/head/head.c
	$(CC) -o $@ $(CFLAGS) src/head/head.c

ungol.iso: $(BINDIR)/iconv
$(BINDIR)/iconv: src/iconv/iconv.c
	$(CC) -o $@ $(CFLAGS) src/iconv/iconv.c

ungol.iso: $(BINDIR)/id
$(BINDIR)/id: src/id/id.c
	$(CC) -o $@ $(CFLAGS) src/id/id.c

ungol.iso: $(BINDIR)/init
$(BINDIR)/init: src/init/init.c
	$(CC) -o $@ $(CFLAGS) src/init/init.c

include mk/kmod.mk

ungol.iso: $(BINDIR)/link
$(BINDIR)/link: src/link/link.c
	$(CC) -o $@ $(CFLAGS) src/link/link.c

include mk/linux.mk

ungol.iso: $(BINDIR)/ln
$(BINDIR)/ln: src/ln/ln.c
	$(CC) -o $@ $(CFLAGS) src/ln/ln.c

ungol.iso: $(BINDIR)/locale
$(BINDIR)/locale: src/locale/locale.c
	$(CC) -o $@ $(CFLAGS) src/locale/locale.c

ungol.iso: $(BINDIR)/logger
$(BINDIR)/logger: src/logger/logger.c
	$(CC) -o $@ $(CFLAGS) src/logger/logger.c

include mk/login.mk

ungol.iso: $(BINDIR)/logname
$(BINDIR)/logname: src/logname/logname.c
	$(CC) -o $@ $(CFLAGS) src/logname/logname.c

ungol.iso: $(BINDIR)/ls
$(BINDIR)/ls: src/ls/ls.c
	$(CC) -o $@ $(CFLAGS) src/ls/ls.c

ungol.iso: $(BINDIR)/minils
$(BINDIR)/minils: src/minils/minils.c
	$(CC) -o $@ $(CFLAGS) src/minils/minils.c

ungol.iso: $(BINDIR)/mkdir
$(BINDIR)/mkdir: src/mkdir/mkdir.c
	$(CC) -o $@ $(CFLAGS) src/mkdir/mkdir.c

ungol.iso: $(BINDIR)/mkfifo
$(BINDIR)/mkfifo: src/mkfifo/mkfifo.c
	$(CC) -o $@ $(CFLAGS) src/mkfifo/mkfifo.c

ungol.iso: $(BINDIR)/more
$(BINDIR)/more: src/more/more.c
	$(CC) -o $@ $(CFLAGS) src/more/more.c

ungol.iso: $(BINDIR)/nice
$(BINDIR)/nice: src/nice/nice.c
	$(CC) -o $@ $(CFLAGS) src/nice/nice.c

ungol.iso: $(BINDIR)/nohup
$(BINDIR)/nohup: src/nohup/nohup.c
	$(CC) -o $@ $(CFLAGS) src/nohup/nohup.c

ungol.iso: $(BINDIR)/od
$(BINDIR)/od: src/od/od.c
	$(CC) -o $@ $(CFLAGS) src/od/od.c

ungol.iso: $(BINDIR)/paste
$(BINDIR)/paste: src/paste/paste.c
	$(CC) -o $@ $(CFLAGS) src/paste/paste.c

ungol.iso: $(BINDIR)/pathchk
$(BINDIR)/pathchk: src/pathchk/pathchk.c
	$(CC) -o $@ $(CFLAGS) src/pathchk/pathchk.c

ungol.iso: $(BINDIR)/renice
$(BINDIR)/renice: src/renice/renice.c
	$(CC) -o $@ $(CFLAGS) src/renice/renice.c

ungol.iso: $(BINDIR)/respawnd
$(BINDIR)/respawnd: src/respawnd/respawnd.c
	$(CC) -o $@ $(CFLAGS) src/respawnd/respawnd.c

ungol.iso: $(BINDIR)/rm
$(BINDIR)/rm: src/rm/rm.c
	$(CC) -o $@ $(CFLAGS) src/rm/rm.c

ungol.iso: $(BINDIR)/rmdir
$(BINDIR)/rmdir: src/rmdir/rmdir.c
	$(CC) -o $@ $(CFLAGS) src/rmdir/rmdir.c

include mk/sh.mk

ungol.iso: $(BINDIR)/sleep
$(BINDIR)/sleep: src/sleep/sleep.c
	$(CC) -o $@ $(CFLAGS) src/sleep/sleep.c

ungol.iso: $(BINDIR)/strings
$(BINDIR)/strings: src/strings/strings.c
	$(CC) -o $@ $(CFLAGS) src/strings/strings.c

ungol.iso: $(BINDIR)/stty
$(BINDIR)/stty: src/stty/stty.c
	$(CC) -o $@ $(CFLAGS) src/stty/stty.c

include mk/syslinux.mk

ungol.iso: $(BINDIR)/tail
$(BINDIR)/tail: src/tail/tail.c
	$(CC) -o $@ $(CFLAGS) src/tail/tail.c

ungol.iso: $(BINDIR)/tput
$(BINDIR)/tput: src/tput/tput.c
	$(CC) -o $@ $(CFLAGS) src/tput/tput.c

ungol.iso: $(BINDIR)/tr
$(BINDIR)/tr: src/tr/tr.c
	$(CC) -o $@ $(CFLAGS) src/tr/tr.c

ungol.iso: $(BINDIR)/uname
$(BINDIR)/uname: src/uname/uname.c
	$(CC) -o $@ $(CFLAGS) src/uname/uname.c

ungol.iso: $(BINDIR)/unlink
$(BINDIR)/unlink: src/unlink/unlink.c
	$(CC) -o $@ $(CFLAGS) src/unlink/unlink.c

ungol.iso: $(BINDIR)/user
$(BINDIR)/user: src/user/user.c
	$(CC) -o $@ $(CFLAGS) src/user/user.c

ungol.iso: $(BINDIR)/uudecode
$(BINDIR)/uudecode: src/uudecode/uudecode.c
	$(CC) -o $@ $(CFLAGS) src/uudecode/uudecode.c

ungol.iso: $(BINDIR)/uuencode
$(BINDIR)/uuencode: src/uuencode/uuencode.c
	$(CC) -o $@ $(CFLAGS) src/uuencode/uuencode.c

ungol.iso: $(BINDIR)/wc
$(BINDIR)/wc: src/wc/wc.c
	$(CC) -o $@ $(CFLAGS) src/wc/wc.c

ungol.iso: $(BINDIR)/who
$(BINDIR)/who: src/who/who.c
	$(CC) -o $@ $(CFLAGS) src/who/who.c

ungol.iso: $(BINDIR)/write
$(BINDIR)/write: src/write/write.c
	$(CC) -o $@ $(CFLAGS) src/write/write.c

