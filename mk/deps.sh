#!/bin/sh

exec > $(dirname $0)/deps.mk
cat <<EOH
.POSIX:

default:
	@echo "Do not use this Makefile directly, it is included from the top-level"

EOH

for i in $(dirname $0)/../src/*; do
	if [ -f $(dirname $0)/$(basename $i).mk ]; then
		printf 'include mk/%s\n\n' $(basename $i).mk
	elif [ -f "$i/Makefile" ]; then
		: do nothing for now
	else
		util=$(basename $i)
		printf 'ungol.iso: $(BINDIR)/%s\n' $util
		printf '$(BINDIR)/%s: src/%s/%s.c\n' $util $util $util
		printf '\t$(CC) -o $@ $(CFLAGS) src/%s/%s.c\n' $util $util
		printf '\n'
	fi
done
