.POSIX:

root/boot/vmlinuz-$(LINUX_VER): src/linux/tar-install/boot/vmlinuz-$(LINUX_VER)
	-mkdir -p root
	cp -far src/linux/tar-install/* root/
	rm -f root/boot/vmlinux-$(LINUX_VER)
	touch $@

src/linux/tar-install/boot/vmlinuz-$(LINUX_VER):
	cd src/linux; make defconfig dir-pkg

linux-clean: clean
	cd src/linux; make clean

ungol.iso: root/boot/vmlinuz-$(LINUX_VER)
