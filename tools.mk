.POSIX:

CC=gcc -static
CFLAGS=-Wall -Wextra
LDFLAGS=-static
BINDIR=root/bin

$(BINDIR)/$(COMPLEX_BIN): src/$(COMPLEX_BIN)/$(COMPLEX_BIN)
	-mkdir -p $(@D)
	cp -f src/$(@F)/$(@F) $@

src/$(COMPLEX_BIN)/$(COMPLEX_BIN):
	-cd $(@D); make CC="$(CC)" CFLAGS="$(CFLAGS)" LDFLAGS="$(LDFLAGS)"
